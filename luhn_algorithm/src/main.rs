
pub fn luhn_sum(n: u32) -> u32 {
    let mut sum = 0;
    if n > 9 {
        for n in n.to_string().chars() {
            let n_int = n.to_digit(10).unwrap();
            sum += n_int;
        }
        sum
    } else {
        n
    }
}

pub fn lunh_check(number: &str) -> Result<Vec<u32>, String> {
    let mut main_container: Vec<u32> = Vec::with_capacity(number.len());
    // Do the moving check from left to right.
    for (i, n) in number.chars().rev().enumerate() {
        // Whether we are dealing with an even or uneven number.
        let modulo = ((i % 2) + 2) % 2;
        let n_number = n.to_digit(10).ok_or("The value contains a string")?;
        if modulo == 1 {
            dbg!(format!("doubling {n_number}"));
            let n_doubled = n_number * 2;
            let n_number = luhn_sum(n_doubled);
            main_container.push(n_number);
        } else {
            main_container.push(n_number);
        }
    }
    Ok(main_container)
}

/// # Luhn's algorithm for verification of credit card numbers.
/// A check to determine whether credit cards numbers are good or not.
/// ## Parameters
/// - `cc_number: &str` -- The credit card number to be checked.
/// # Returns
/// `bool` -- `true` when the number is valid, `false` other wise.
pub fn luhn(cc_number: &str) -> bool {
    
    // Step one, length check
    let number = cc_number.replace(" ", "");
    if number.len() < 2 {
        return false;
    }
    
    
    let main_container_result = lunh_check(&number);
    if main_container_result.is_ok() {
        let main_container = main_container_result.unwrap();
        dbg!(&main_container);
        let sum: u32 = main_container.iter().sum();
        dbg!(&sum);
        let sum_string = sum.to_string();
        dbg!(&sum_string);
        if sum_string.ends_with("0") {
            true
        } else {
            false
        }
    } else {
        false
    }
}

#[test]
fn test_lunh_sum() {
    assert_eq!(luhn_sum(4), 4);
    assert_eq!(luhn_sum(10), 1);
}

#[test]
fn test_non_digit_cc_number() {
    assert!(!luhn("foo"));
    assert!(!luhn("foo 0 0"));
}

#[test]
fn test_empty_cc_number() {
    assert!(!luhn(""));
    assert!(!luhn(" "));
    assert!(!luhn("  "));
    assert!(!luhn("    "));
}

#[test]
fn test_single_digit_cc_number() {
    assert!(!luhn("0"));
}

#[test]
fn test_two_digit_cc_number() {
    assert!(luhn(" 0 0 "));
}

#[test]
fn test_valid_cc_number() {
    assert!(luhn("4263 9826 4026 9299"));
    assert!(luhn("4539 3195 0343 6467"));
    assert!(luhn("7992 7398 713"));
}

#[test]
fn test_invalid_cc_number() {
    assert!(!luhn("4223 9826 4026 9299"));
    assert!(!luhn("4539 3195 0343 6476"));
    assert!(!luhn("8273 1232 7352 0569"));
}

#[allow(dead_code)]
fn main() { 
}
